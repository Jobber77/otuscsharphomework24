﻿using BenchmarkDotNet.Attributes;
using Faker;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Homework24
{
    public class Benchmark
    {
        IEnumerable<int> ThousandList => GetTestData(1_000);
        IEnumerable<int> MillionList => GetTestData(1_000_000);
        IEnumerable<int> TenMillionList => GetTestData(1_000_000);
        public Benchmark() { }
        
        [Benchmark]
        public int Test_1000_Sync() => ThousandList.Sum();

        [Benchmark]
        public int Test_1000_PLINQ() => ThousandList.AsParallel().Sum();

        [Benchmark]
        public async Task<int> Test_1000_Multithread() => await CalcMultithread(ThousandList);

        [Benchmark]
        public int Test_1000000_Sync() => MillionList.Sum();

        [Benchmark]
        public int Test_1000000_PLINQ() => MillionList.AsParallel().Sum();

        [Benchmark]
        public async Task<int> Test_1000000_Multithread() => await CalcMultithread(MillionList);

        [Benchmark]
        public int Test_10000000_Sync() => TenMillionList.Sum();

        [Benchmark]
        public int Test_10000000_PLINQ() => TenMillionList.AsParallel().Sum();

        [Benchmark]
        public async Task<int> Test_10000000_Multithread() => await CalcMultithread(TenMillionList);

        private IEnumerable<int> GetTestData(int amountOfRecords)
        {
            for (var i = 0; i < amountOfRecords; i++)
                yield return RandomNumber.Next(100);
        }

        private async Task<int> CalcMultithread(IEnumerable<int> list)
        {
            var procCount = Environment.ProcessorCount;
            var page = list.Count() / procCount;
            var tasks = new List<Task<int>>();
            for (var i = 0; i < procCount; i++)
                tasks.Add(Task.Run(() => list.Skip(page * i).Sum()));
            await Task.WhenAll();
            return tasks.Select(task => task.Result).Sum();
        }
    }
}
