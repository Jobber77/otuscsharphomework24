﻿using BenchmarkDotNet.Configs;
using BenchmarkDotNet.Running;

namespace Homework24
{
    class Program
    {
        static void Main(string[] args) =>
            BenchmarkRunner.Run<Benchmark>(DefaultConfig.Instance.With(ConfigOptions.DisableOptimizationsValidator));
    }
}
